package edu.hawaii;

import java.util.Scanner;

/**
 * A simple calculator.
 * 
 * @author Branden Ogata
 */
public class CalculatorDemo {
  /**
   * Prints the results of calculations.
   * @param args    The String[] containing command line arguments; not used.
   */
  public static void main(String[] args) {
    System.out.println("Enter a mathematical expression: ");
    Scanner input = new Scanner(System.in);
    String expression = input.nextLine().trim();
                  
    String[] components = expression.split(" ");
    double firstOperand = Double.parseDouble(components[0]);
    String operator = components[1];
    double secondOperand = Double.parseDouble(components[2]);
    
    System.out.format("%.2f %s %.2f%n", 
                      firstOperand,
                      operator,
                      secondOperand);    
    
    input.close();  
  }
}
